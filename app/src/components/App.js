import React, { Component } from 'react';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            distance: 0
        }
    }

    componentDidMount() {
        this.initMapComponents();
    }

    initMapComponents() {
        this.drawMap();
        this.drawAutocomplete();
        this.drawPoly();
    }

    drawPoly() {
        const lineSymbol = {
            path: 'M 0,-1 0,1',
            strokeOpacity: 1,
            strokeColor: 'red',
            strokeWidth: 2,
            scale: 3
        };
        this.flightPath = new google.maps.Polyline({
            map: this.map,
            strokeOpacity: 0,
            icons: [{
                icon: lineSymbol,
                offset: '0',
                repeat: '20px'
            }],
            path: []
        });
    }

    drawAutocomplete() {
        this.setMarkers();
        const options = {
            componentRestrictions: {'country': 'us'},
            types: ['(cities)']
        }
        const input1 = document.getElementById('locationTextField1');
        const autocomplete1 = new google.maps.places.Autocomplete(input1, options);
        const input2 = document.getElementById('locationTextField2', options);
        const autocomplete2 = new google.maps.places.Autocomplete(input2);

        autocomplete1.addListener('place_changed', () => {
            this.pointA = autocomplete1.getPlace();
            this.markerA.setMap(this.map);
            this.markerA.setPosition({lat:this.pointA.geometry.location.lat(), lng:this.pointA.geometry.location.lng()});
            this.calculateDistance();
        });
        autocomplete2.addListener('place_changed', () => {
            this.pointB = autocomplete2.getPlace();
            this.markerB.setMap(this.map);
            this.markerB.setPosition({lat:this.pointB.geometry.location.lat(), lng:this.pointB.geometry.location.lng()});
            this.calculateDistance();
        });
    }

    setMarkers() {
        this.markerA  = new google.maps.Marker({
            draggable: true,
            animation: google.maps.Animation.DROP,
        });
        this.markerB =  new google.maps.Marker({
            draggable: true,
            animation: google.maps.Animation.DROP,
        });
    }

    calculateDistance() {
        if(this.pointA && this.pointB) {
            this.drawRoute();
            this.setState({distance:this.getDistance(this.pointA.geometry.location, this.pointB.geometry.location)});
        }
    }

    drawRoute() {
        this.flightPath.setPath([
            {lat:this.pointA.geometry.location.lat(), lng:this.pointA.geometry.location.lng()},
            {lat:this.pointB.geometry.location.lat(), lng:this.pointB.geometry.location.lng()}
            ]);
    }

    getDistance(p1, p2) {
        const rad = function(x) {
            return x * Math.PI / 180;
        };
        const R = 6378137/1609.344; // Earth’s mean radius in miles
        const dLat = rad(p2.lat() - p1.lat());
        const dLong = rad(p2.lng() - p1.lng());
        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const d = R * c;
        return Math.round(d); // returns the distance in miles
    }

    drawMap() {
        const myOptions = {
            center: {lat: 37.1, lng: -95.7},
            zoom: 4,
            mapTypeControl: false,
            zoomControl: false,
            streetViewControl: false
        };
        this.map =  new google.maps.Map(document.getElementById("map"), myOptions);
       }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="form-group col-md-6">
                        <label htmlFor="locationTextField1">Location A</label>
                        <input className="form-control" id="locationTextField1" type="text" size="50" />
                    </div>

                    <div className="form-group col-md-6">
                        <label htmlFor="locationTextField2">Location B</label>
                        <input className="form-control" id="locationTextField2" type="text" size="50" />
                    </div>
                </div>

                <div className="distance">Distance: {this.state.distance } miles</div>
            <div id="map" className="map"></div>
            </div>
    );
    }
}